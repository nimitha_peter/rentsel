package testmethods;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageobjects.CreateListingDetailPage;
import pageobjects.HomePage;
import pageobjects.RentCreateSecPage;
import pageobjects.SaleCreateSecPage;
import pageobjects.SignInPage;
import utility.ExcelRead;
import utility.Log;
import utility.PropertyFileRead;

public class CreateListingRentSaleTest extends base.Driver{
	
	PropertyFileRead read = new PropertyFileRead();
	ExcelRead excel = new ExcelRead();
	
	
	@Test(dataProvider = "RentSale")
	public void RentSale(String username, String Password, String Title, String Address,String qnty,String securitydep,String pricehr,String priceweek,String priceday,String pricemonth,String salequantity,String saleprice,String monthlypay,String usmail) 
	{
	try {
		open(getPageURL());
		SignInPage hp = new SignInPage(driver);
		hp.LogIn(username, Password);
		HomePage cp = new HomePage(driver);
		cp.CreateListing();
		CreateListingDetailPage llp = new CreateListingDetailPage(driver);
		llp.MyListingItem();
		llp.MyListingItemRentandSale();
		llp.categoryRent(Title);
		llp.Item(Address);
		RentCreateSecPage pp = new RentCreateSecPage(driver);
		pp.RentSecPage(qnty,securitydep,pricehr,priceweek,priceday,pricemonth);
		
		Thread.sleep(3000);
		SaleCreateSecPage scp = new SaleCreateSecPage(driver);
		scp.Duration();
		scp.SaleSecPage(salequantity, saleprice, monthlypay, usmail);
		scp.SubmitBtn();
	
		
		
		
		
		
		
	}
	catch(Exception e) 
	{
		Log.addMessage("sign In failed");
		Assert.assertTrue(false, "Sign In failed");
		e.printStackTrace();
	}
	}
	
	@DataProvider(name = "RentSale")
	public Object[][] mixed() throws Exception {
		return excel.getTableArray(InputData, "TestData", "RentSale");
	}

}

