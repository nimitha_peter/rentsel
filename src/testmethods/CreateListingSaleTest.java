package testmethods;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageobjects.CreateListingDetailPage;
import pageobjects.HomePage;
import pageobjects.RentCreateSecPage;
import pageobjects.SaleCreateSecPage;
import pageobjects.SignInPage;
import utility.ExcelRead;
import utility.Log;
import utility.PropertyFileRead;

public class CreateListingSaleTest extends base.Driver {
	
	PropertyFileRead read = new PropertyFileRead();
	ExcelRead excel = new ExcelRead();
	
	
	@Test(dataProvider = "SaleData")
	public void SaleTest(String username, String Password, String Title, String Address,String salequantity,String saleprice,String monthlypay,String usmail) 
	{
	try {
		open(getPageURL());
		SignInPage hp = new SignInPage(driver);
		hp.LogIn(username, Password);
		HomePage cp = new HomePage(driver);
		cp.CreateListing();
		CreateListingDetailPage llp = new CreateListingDetailPage(driver);
		llp.MyListingItem();
		llp.MyListingItemSale();
		llp.categoryRent(Title);
		llp.Item(Address);
		SaleCreateSecPage pp = new SaleCreateSecPage(driver);
		pp.ImageUpload();
		pp.SaleSecPage(salequantity, saleprice, monthlypay, usmail);
		pp.Duration();
		pp.SubmitBtn();
		
		
		
	}
	catch(Exception e) 
	{
		Log.addMessage("sign In failed");
		Assert.assertTrue(false, "Sign In failed");
		e.printStackTrace();
	}
	}
	
	@DataProvider(name = "SaleData")
	public Object[][] mixed() throws Exception {
		return excel.getTableArray(InputData, "TestData", "SaleData");
	}
}
	


