package testmethods;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageobjects.SignInPage;
import utility.ExcelRead;
import utility.Log;
import utility.PropertyFileRead;

public class SignInTest extends base.Driver {
	
	PropertyFileRead read = new PropertyFileRead();
	ExcelRead excel = new ExcelRead();
	
	
	@Test(dataProvider = "item")
	public void LoginTest(String username, String Password) {
	try {
		open(getPageURL());
		SignInPage hp = new SignInPage(driver);
		hp.LogIn(username, Password);
		
	}catch(Exception e) {
		Log.addMessage("sign In failed");
		Assert.assertTrue(false, "Sign In failed");
		e.printStackTrace();
	}
	}
	
	@DataProvider(name = "item")
	public Object[][] mixed() throws Exception {
		return excel.getTableArray(InputData, "TestData", "UserLogin");
	}
}
