package testmethods;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;



import pageobjects.CreateListingDetailPage;
import pageobjects.HomePage;
import pageobjects.RentCreateSecPage;
import pageobjects.SaleCreateSecPage;
import pageobjects.Service;
import pageobjects.SignInPage;
import utility.ExcelRead;
import utility.Log;
import utility.PropertyFileRead;

public class ServicesTest extends base.Driver{
	
	PropertyFileRead read = new PropertyFileRead();
	ExcelRead excel = new ExcelRead();
	
	
	@Test(dataProvider = "ServiceData")
	
	public void Services(String username, String Password, String Title, String Address,String addon,String servcprice) 
	{
	try {
		open(getPageURL());
		SignInPage hp = new SignInPage(driver);
		hp.LogIn(username, Password);
		HomePage cp = new HomePage(driver);
		cp.CreateListing();
		CreateListingDetailPage llp = new CreateListingDetailPage(driver);
		//llp.MyListingItem();
	//	llp.MyListingItemRent();
		llp.categoryService(Title);
		llp.Item(Address);
		SaleCreateSecPage pp = new SaleCreateSecPage(driver);
		pp.ImageUpload();
		pp.Duration();
		
		Service sv=new Service(driver);
		sv.servicepage(addon,servcprice);
		
		
				
	
	//	pp.RentSecPage(qnty,securitydep,pricehr,priceweek,priceday,pricemonth);
	//	pp.SubmitBtn();
		
		
		
	}
	catch(Exception e) 
	{
		Log.addMessage("sign In failed");
		Assert.assertTrue(false, "Sign In failed");
		e.printStackTrace();
	}
	}
	@DataProvider(name = "ServiceData")
	public Object[][] mixed() throws Exception {
		return excel.getTableArray(InputData, "TestData", "ServiceData");
	}


}
