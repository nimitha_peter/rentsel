package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.internal.thread.TestNGThread;

import utility.CommonFunctions;
import utility.Log;
import base.Driver;

public class HomePage extends Driver{
	@FindBy(xpath="//button[contains(.,'CREATE A LISTING')]")
	WebElement CreateListingBtn;
	
	@FindBy(id ="email")
	WebElement userName;
	
	@FindBy(id ="password")
	WebElement passWord;
	
	@FindBy(xpath="//button[@class='btn btn-success']")
	WebElement LoginBtn;
	
	
	CommonFunctions cmn = new CommonFunctions(driver);

	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void CreateListing() {
		try {
			Thread.sleep(5000);
			CreateListingBtn.click();
			Log.addMessage("Create Listing Button clicked");
						
		} catch (Exception e) {
			Log.addMessage("Something went wrong in Create Listing Button");
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false, "Something went wrong in Create Listing Button");
		}
	}

}
