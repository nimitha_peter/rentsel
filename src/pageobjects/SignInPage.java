package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.internal.thread.TestNGThread;

import utility.CommonFunctions;
import utility.Log;
import base.Driver;

public class SignInPage extends Driver{
	@FindBy(xpath="//button[@class='login-btn']")
	WebElement LoginButton;
	
	@FindBy(id ="email")
	WebElement userName;
	
	@FindBy(id ="password")
	WebElement passWord;
	
	@FindBy(xpath="//button[@class='btn btn-success']")
	WebElement LoginBtn;
	
	
	CommonFunctions cmn = new CommonFunctions(driver);

	public SignInPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void LogIn(String username,String Password) {
		try {
			LoginButton.click();
			userName.clear();
			Thread.sleep(5000);
			userName.sendKeys(username);
			Log.addMessage("Username entered");
			passWord.clear();
			passWord.sendKeys(Password);
			Log.addMessage("Password entered");
			LoginBtn.click();
			Log.addMessage("User successfully logged in");
			
		} catch (Exception e) {
			Log.addMessage("Failed to logIn");
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false, "User failed to login");
		}
	}

}
