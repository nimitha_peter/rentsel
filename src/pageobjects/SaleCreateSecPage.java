package pageobjects;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import utility.Log;

public class SaleCreateSecPage extends base.Driver{
	@FindBy(xpath  = "//label[@for='file-upload']")
	WebElement BrowseFiles;
	
	@FindBy(xpath="//select[@formcontrolname='listingDuration']")
	WebElement Duration;
	
	@FindBy(xpath="//select[@formcontrolname='saleCondition']")
	WebElement SaleCondition;
	
	
	@FindBy(xpath="//select[@formcontrolname='saleAggrement']")
	WebElement SaleAggrement;
	
	@FindBy(xpath="//input[@formcontrolname='saleQuantity']")
	WebElement SaleQuantity;
	
	@FindBy(xpath="//input[@formcontrolname='salePrice']")
	WebElement SalePrice;
	
	@FindBy(xpath="//span[contains(.,'Accepts monthly payment')]")
	WebElement MonthlyPayment;
	
	@FindBy(xpath="//input[@formcontrolname='saleMonths']")
	WebElement InputMonthlyPayment;
	
	@FindBy(xpath="//span[contains(.,'ALLOW MAKE OFFER')]")
	WebElement AllowMakeOffer;
	
	@FindBy(xpath="//div[@class='col-12 col-md-6 form-group time-zone ng-star-inserted'][1]//input[@name='shipping-name']")
	WebElement USMAilRadBtn;
	
	@FindBy(xpath="//div[@class='col-12 col-md-6 form-group time-zone ng-star-inserted'][1]//input[@class='form-control textbox-style ng-untouched ng-pristine ng-valid']")
	WebElement USMAilField;
	
	
	@FindBy(xpath="//button[@class='common-button medium-width success']")
	WebElement SubmitButton;
	
/*	@FindBy(xpath="//span[contains(.,'ALLOW MAKE OFFER')]")
	WebElement AllowMakeOffer;
	
	@FindBy(xpath="//span[contains(.,'ALLOW MAKE OFFER')]")
	WebElement AllowMakeOffer;
	
	@FindBy(xpath="//span[contains(.,'ALLOW MAKE OFFER')]")
	WebElement AllowMakeOffer;
	
	@FindBy(xpath="//span[contains(.,'ALLOW MAKE OFFER')]")
	WebElement AllowMakeOffer;
	
	@FindBy(xpath="//span[contains(.,'ALLOW MAKE OFFER')]")
	WebElement AllowMakeOffer;
	
	@FindBy(xpath="//span[contains(.,'ALLOW MAKE OFFER')]")
	WebElement AllowMakeOffer;
	
	@FindBy(xpath="//span[contains(.,'ALLOW MAKE OFFER')]")
	WebElement AllowMakeOffer;
	*/
	
	
	public SaleCreateSecPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	public void ImageUpload() {
		try
	 {
			Thread.sleep(5000);
			BrowseFiles.click();
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyPress(KeyEvent.VK_ENTER);
			Log.addMessage("Image Uploaded");
			
	 }
		catch (Exception e) {
			Log.addMessage("Something went wrong while uploading image");
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false, "Something went wrong in Sale Item second Page");
		}
	}
			
	public void Duration() {
		try {
			Thread.sleep(3000);
			Select duration = new Select(Duration);
			duration.selectByVisibleText("Monthly");
			Log.addMessage("Duration selected from the dropdown..");
			
			Thread.sleep(2000);
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_Z);
			Log.addMessage("Ctrl+Z is clicked..");
		}
		catch (Exception e) {
			Log.addMessage("Something went wrong while selecting duration");
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false, "Something went wrong while selecting duration");
		}
	}
		
			public void SaleSecPage(String salequantity,String saleprice,String monthlypay,String usmail) {
				try {	
			
			
			Select sale = new Select(SaleCondition);
			sale.selectByVisibleText("Manufacturer Refurbished");
	        Log.addMessage("Sales condition is selected..");
			
			//SaleCondition.sendKeys(salecondition);
	        
	        Thread.sleep(2000);
	        Select saleaggri = new Select(SaleAggrement);
	        saleaggri.selectByVisibleText("Sale 1");
	        Log.addMessage("SAles aggrement is selected..");
	        
			//SaleAggrement.sendKeys(saleAggriment);
			Thread.sleep(2000);
	        SaleQuantity.sendKeys(salequantity);
			Log.addMessage("Sales quantity is entered");
			
			
			Thread.sleep(2000);
			SalePrice.sendKeys(saleprice);
			Log.addMessage("Sales price is eneterd..");
			
			Thread.sleep(2000);
			MonthlyPayment.click();
			Log.addMessage("SAles monthly payment is cklicked");
			
			
			Thread.sleep(2000);
			InputMonthlyPayment.sendKeys(monthlypay);
			Log.addMessage("SAles monthly payment is entered..");
			
			Thread.sleep(2000);
			AllowMakeOffer.click();
			Log.addMessage("Allow make offer is clicked..");
			
			Thread.sleep(2000);
			USMAilRadBtn.click();
			Log.addMessage("US mail button in clicked..");
			
			Thread.sleep(3000);
			USMAilField.sendKeys(usmail);
			Log.addMessage("US mail is entered..");
			
			
		}
		catch (Exception e) {
			Log.addMessage("Something went wrong in Sale Item second Page");
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false, "Something went wrong in Sale Item second Page");
		}
	}
	
	public void SubmitBtn() {
		try {
			Thread.sleep(2000);
			SubmitButton.click();
			Log.addMessage("Submit button is clicked..");
	
}
		catch (Exception e) {
			Log.addMessage("Something went wrong while clicking submit button");
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false, "Something went wrong in Rent Item second Page");
		}}

}
