package pageobjects;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import utility.CommonFunctions;
import utility.Log;

public class Service extends base.Driver {
	
	@FindBy(xpath="//input[@formcontrolname='serviceAddonFees']")
	WebElement AddOnFee;
	
	@FindBy(xpath="//input[@formcontrolname='servicePricePerHour']")
	WebElement Serviceprice;
	
	@FindBy(xpath="//select[@formcontrolname='serviceAgreement']")
	WebElement Serviceagreement;
	
	@FindBy(xpath="//button[@class='common-button medium-width success']")
	WebElement SubmitButton;
	
	
	CommonFunctions cmn = new CommonFunctions(driver);
	
	public Service(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	public void servicepage(String addon,String servcprice) {
		try
	 {
			Thread.sleep(5000);
			AddOnFee.sendKeys(addon);
			
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyPress(KeyEvent.VK_ENTER);
			Log.addMessage("Image Uploaded");
			
			Serviceprice.sendKeys(servcprice);
			Log.addMessage("Service price entered");
			
			Select sale = new Select(Serviceagreement);
			sale.selectByVisibleText("Service 1");
			Log.addMessage("Service agreement entered");
			
			SubmitButton.click();
			Log.addMessage("Submit button is clicked..");
			
			
			
	 }
		catch (Exception e) {
			Log.addMessage("Something went wrong while uploading image");
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false, "Something went wrong in Sale Item second Page");
		}
	}

	
	public void SubmitBtn() {
		try {
			Thread.sleep(2000);
			SubmitButton.click();
			Log.addMessage("Submit button is clicked..");
	
}
		catch (Exception e) {
			Log.addMessage("Something went wrong while clicking submit button");
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false, "Something went wrong in Rent Item second Page");
		}}
}
