package pageobjects;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.internal.thread.TestNGThread;

import utility.CommonFunctions;
import utility.Log;
import base.Driver;

public class RentCreateSecPage extends Driver{
	@FindBy(xpath  = "//label[@for='file-upload']")
	WebElement BrowseFiles;
	
	// ChooseBtn.sendKeys("C:/Users/Nimitha/Downloads/ddsa.jpg");
	//form-control textbox-style select-style ng-pristine ng-invalid ng-touched
	
	@FindBy(xpath="//select[@formcontrolname='listingDuration']")
	WebElement Duration;
	
	
	@FindBy(xpath="//option[contains(.,'Monthly')]")
	WebElement DurationOption;
	
	@FindBy(xpath="//input[@formcontrolname='rentQuanity']")
	WebElement Qty;
	
	@FindBy(xpath="//input[@formcontrolname='securityDeposit']")
	WebElement Securitydeposit;
	
	@FindBy(xpath="//input[@formcontrolname='pricePerHour']")
	WebElement PricePerHour;
	
	@FindBy(xpath="//input[@formcontrolname='pricePerWeek']")
	WebElement PricePerweek;
	
	@FindBy(xpath="//input[@formcontrolname='pricePerday']")
	WebElement PricePerDay;
	
	@FindBy(xpath="//input[@formcontrolname='pricePerMonth']")
	WebElement Pricepermonth;
	
	@FindBy(xpath="//select[@formcontrolname='refundPolicy']")
	WebElement RefundPolicy;
	
	@FindBy(xpath="//button[contains(.,' SUBMIT ')]")
	WebElement Submit;
	
	
	
	
	//input[@formcontrolname='securityDeposit']
	
	
	
	CommonFunctions cmn = new CommonFunctions(driver);

	public RentCreateSecPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void RentSecPage(String qnty,String securitydep,String pricehr,String priceweek,String priceday,String pricemonth) {
		try {
			Thread.sleep(5000);
			BrowseFiles.click();
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyPress(KeyEvent.VK_ENTER);
			Log.addMessage("Image Uploaded");
			
			
			Thread.sleep(3000);
			Select duration = new Select(Duration);
			duration.selectByVisibleText("Monthly");
			Log.addMessage("Duration selected from the dropdown..");
			
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_Z);
			Log.addMessage("Control+Z is pressed-Because Dropdown is blinking..");
			
			
			//DurationOption.click();
			
			
			Qty.sendKeys(qnty);
			Log.addMessage("Quantity is entered..");
			
			Securitydeposit.sendKeys(securitydep);
			Log.addMessage("Security deposit is entered..");
			
			PricePerHour.sendKeys(pricehr);
			Log.addMessage("Price per hour is entered..");
			
			PricePerweek.sendKeys(priceweek);
			Log.addMessage("Price per week is entered..");
			
			PricePerDay.sendKeys(priceday);
			Log.addMessage("price per day is entered..");
			
		//	Pricepermonth.sendKeys(pricemonth);
		//	Log.addMessage("Price per month is entered..");
			
			Thread.sleep(5000);
			Select refund = new Select(RefundPolicy);
			refund.selectByVisibleText("Moderate");
			Log.addMessage("Refund policy is selected from the dropdown..");
			
			
			
		
				
		} catch (Exception e) {
			Log.addMessage("Something went wrong in Rent Item second Page");
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false, "Something went wrong in Rent Item second Page");
		}
	}
	
	
	public void SubmitBtn() {
		try {
	Thread.sleep(2000);
	Submit.click();
	Log.addMessage("Submitt button is clicked..");
	
}
		catch (Exception e) {
			Log.addMessage("Something went wrong while clicking submit button");
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false, "Something went wrong in Rent Item second Page");
		}}}
