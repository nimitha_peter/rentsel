package pageobjects;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.internal.thread.TestNGThread;



import utility.CommonFunctions;
import utility.Log;
import base.Driver;

public class CreateListingDetailPage extends Driver{
	@FindBy(xpath  = "//label[contains(.,'Item')]")
	WebElement Item;
	

	
	@FindBy(xpath="//input[@formcontrolname='subItem'][1]")
	WebElement RentBtn;
	
	@FindBy(xpath="//input[@formcontrolname='subItem'][2]")
	WebElement SaleBtn;
	
//	@FindBy(xpath="//span[contains(.,'Make this item available for sale')]")
//	WebElement SaleBtn;
	
	@FindBy(xpath="//input[@formcontrolname='subItem'][3]")
	WebElement RentandSaleBtn;
	
	
	
	@FindBy(xpath="//input[@formcontrolname='title']")
	WebElement Titlename;
	
	@FindBy(xpath="//div[@class='form-group col-md-6 col-sm-12']//select")
	WebElement CategorySelect;
	
	@FindBy(xpath="//option[contains(.,'Animals')]")
	WebElement OptionSelect;
	
	@FindBy(xpath="//input[@formcontrolname='address']")
	WebElement AddressField;
	
	@FindBy(xpath="//input[@formcontrolname='city']")
	WebElement CityField;
	
	@FindBy(xpath="//input[@formcontrolname='state']")
	WebElement StateField;
	
	@FindBy(xpath="//input[@formcontrolname='country']")
	WebElement CountryField;
	
	@FindBy(xpath="//input[@formcontrolname='zip']")
	WebElement zipField;
	
	@FindBy(xpath="//button[contains(.,'NEXT')]")
	WebElement NextBtn;
	
	CommonFunctions cmn = new CommonFunctions(driver);

	public CreateListingDetailPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void MyListingItem() {
		try {
			Thread.sleep(5000);
			Item.click();
			Log.addMessage("Item clicked");
		}
		catch (Exception e)
		{
			Log.addMessage("Something went wrong in My Listing Item");
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false, "Something went wrong in My Listing Item");
		}
	}
	
	
	public void MyListingItemRent() {
		try {	
			Thread.sleep(5000);
			RentBtn.click();
			Log.addMessage("Rent selected");
		}
			
			catch (Exception e)
			{
				Log.addMessage("Something went wrong in My Listing Item Rent");
				System.out.println(e.getMessage().toString());
				Assert.assertTrue(false, "Something went wrong in My Listing Item Rent");
			}
		}
	
	public void MyListingItemRentandSale() {
		try {	
			Thread.sleep(5000);
			RentandSaleBtn.click();
			Log.addMessage("Rent and sale is selected");
		}
			
			catch (Exception e)
			{
				Log.addMessage("Something went wrong in My Listing Item Rent and sale..");
				System.out.println(e.getMessage().toString());
				Assert.assertTrue(false, "Something went wrong in My Listing Item Rent and sale..");
			}
		}
	
	
	public void MyListingItemSale() {
		try {	
			Thread.sleep(5000);
			SaleBtn.click();
			Log.addMessage("Rent selected");
		}
			
			catch (Exception e)
			{
				Log.addMessage("Something went wrong in My Listing Item Sale");
				System.out.println(e.getMessage().toString());
				Assert.assertTrue(false, "Something went wrong in My Listing Item Sale");
			}
		}
	
	
	
	
	
	public void categoryService(String Title){
		try {
			Titlename.sendKeys(Title);
			Log.addMessage("Title Entered");
			
			
			Select category = new Select(CategorySelect);
			category.selectByVisibleText("Cleaning");
	        Log.addMessage("Category is selected..");
		}
		 catch (Exception e) {
				Log.addMessage("Something went wrong while selecting service category..");
				System.out.println(e.getMessage().toString());
				Assert.assertTrue(false, "Something went wrong while selecting category..");
	}}
			
			public void categoryRent(String Title){
				try {
					Titlename.sendKeys(Title);
					Log.addMessage("Title Entered");
					
					
					Select category = new Select(CategorySelect);
					category.selectByVisibleText("Animals");
			        Log.addMessage("Category is selected..");
				}
				 catch (Exception e) {
						Log.addMessage("Something went wrong while selecting category..");
						System.out.println(e.getMessage().toString());
						Assert.assertTrue(false, "Something went wrong while selecting category..");
			}}
			
			
	        public void Item(String Address) {
				try {
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(AddressField);
			actions1.click();
			actions1.sendKeys(Address);
			Thread.sleep(5000);
			actions1.build().perform();
			Log.addMessage("Address entered");
			Thread.sleep(5000);
			
			 
			 Robot robot = new Robot();
			 robot.keyPress(KeyEvent.VK_DOWN);
			 robot.keyPress(KeyEvent.VK_DOWN);
			 Thread.sleep(2000);
			 robot.keyPress(KeyEvent.VK_ENTER);
			 Log.addMessage("Address is selected from Auto dropdown..");
			 Thread.sleep(5000);
			// zipField.sendKeys(Zip);
			 Thread.sleep(3000);
			 NextBtn.click();
			 Log.addMessage("Next button is clicked..");
			
			 
			 Thread.sleep(5000);
			
			
						
		} catch (Exception e) {
			Log.addMessage("Something went wrong in Rent Item First Page");
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false, "Something went wrong in Rent Item First Page");
		}
	}

}
