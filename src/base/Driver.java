/**
 *Copyright (c) 2014 Qburst Technologies, Inc. All Rights Reserved.
 */
package base;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import utility.PropertyFileRead;
import utility.PropertyUtility;

public class Driver {
	public static WebDriver driver;
	public String pageURL;
	public PropertyFileRead prop = new PropertyFileRead();
	public String environment;
	public String browser;
	public String userDirectory;
	protected static String InputData = PropertyUtility.getProperty("InputData");
	/**
	 * The function to get the url in the browser
	 */

	@SuppressWarnings("deprecation")
	@BeforeClass
	@Parameters({ "browser", "environment"})
	
	public void testOpenUrl(String browser, String environment)
			throws IOException {
		try {
			this.environment=environment;
			DesiredCapabilities capabilities = null;
			Properties sysProps = System.getProperties();
//			browser = sysProps.getProperty("browser");
//			environment = sysProps.getProperty("environment");
			userDirectory = sysProps.getProperty("user.dir");
			DOMConfigurator.configure(userDirectory +"/Log4j/log4j.xml");	
			
			if (browser.equals("chrome")) {
				ChromeOptions options = new ChromeOptions();
				capabilities = DesiredCapabilities.chrome();
				options.addArguments("test-type");

				if(System.getProperty("os.name").startsWith("Windows")) {
					System.setProperty("webdriver.chrome.driver",userDirectory+"/Driver/chromedriver.exe");
				}else {
					System.setProperty("webdriver.chrome.driver",userDirectory+"/Driver/chromedriver");
					}
				
				driver = new ChromeDriver(capabilities);
				driver.manage().deleteAllCookies();

				driver.manage().timeouts()
						.implicitlyWait(20, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				
			} else if (browser.equals("firefox")) {
				System.setProperty("webdriver.gecko.driver",userDirectory+"/Driver/geckodriver");
				driver = new FirefoxDriver();		
				driver.manage().timeouts()
						.implicitlyWait(60, TimeUnit.SECONDS);
				WebElement html = driver.findElement(By.tagName("html"));
				html.sendKeys(Keys.chord(Keys.CONTROL, "0"));
				driver.manage().window().maximize();
			} else if (browser.equals("safari")) {
				capabilities = new DesiredCapabilities();
				capabilities.setPlatform(Platform.WINDOWS);
				driver = new SafariDriver();

			}
			driver.manage().window().maximize();
			//driver.get(getPageURL());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void open(String url) {
		driver.get(url);
	}
	
	String currentURL = "";

	public String getPageURL() {
		String url = null;
		if (environment.equals("production")){
			url = prop.getProperty("prodUrl");
		} else if(environment.equals("test")){
			url = prop.getProperty("testUrl");
		} else if (environment.equals("staging")){
			url = prop.getProperty("stagingUrl");
		}
		//System.out.println("Pageurl is " + prop.getProperty("BaseUrl"));		
		return(url);
	}
	
	
	//----------------------CODE for Sitecheck------------------------------------//
	
	public int getStatusCode(String pageUrl) throws Exception  {
		URL url = new URL(pageUrl);
		HttpURLConnection huc = (HttpURLConnection)url.openConnection();
		huc.setRequestMethod("GET");
		huc.connect();
		return huc.getResponseCode();
}
	

	// @AfterClass
	/**
	 * The function to close the browser
	 */
	public void closeUrl() {

		//driver.quit();
	}

	@AfterSuite
	/**
	 * To ensure that the driver has been closed or not
	 */
	public void tearDown() {
		boolean hasQuit = (driver.toString().contains("null")) ? true : false;
		if (hasQuit == false) {
		//	driver.quit();
		}
	}


}